#addin "nuget:?package=Cake.Gulp&version=0.7.1"
#tool "nuget:?package=GitVersion.CommandLine"

//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////

var target = Argument("target", "Default");

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////

Task("Clean")
    .Does(() =>
    {
        CleanDirectories("dist");
    });

Task("Gulp")
    .Does(() =>
    {
        Gulp.Local.Execute(settings => 
        {
            settings.WithGulpFile("./gulp.js");
        });
    });

Task("Copy-Needed-Files")
    .Does(() =>
    {
        CopyFile("favicon.ico", "dist/favicon.ico");
        CopyFile("robots.txt", "dist/robots.txt");
        CopyFile("sitemap.xml", "dist/sitemap.xml");
        CopyFile("web.config", "dist/web.config");
        CopyDirectory("images", "dist/images");
        CopyDirectory("docs", "dist/docs");
        CopyDirectory("fonts", "dist/fonts");
        CopyDirectory("inc", "dist/inc");
    });

//////////////////////////////////////////////////////////////////////
// TASK TARGETS
//////////////////////////////////////////////////////////////////////

Task("Default")
    .IsDependentOn("Clean")
    .IsDependentOn("Gulp")
    .IsDependentOn("Copy-Needed-Files");

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);