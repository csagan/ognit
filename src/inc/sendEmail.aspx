﻿<script language="C#" runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Form.HasKeys())
        {
            var errors = new List<string>();

            string name = Request.Form["contactName"];
            string email = Request.Form["contactEmail"];
            string subject = Request.Form["contactSubject"];
            string contact_message = Request.Form["contactMessage"];

            // Check Name
            if (string.IsNullOrEmpty(name) || name.Length < 2)
            {
                errors.Add("Please enter your name.");
            }

            // Check Email
            if (!Regex.IsMatch(email, "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$"))
            {
                errors.Add("Please enter a valid email address.");
            }

            // Check Message
            if (string.IsNullOrEmpty(contact_message) || contact_message.Length < 15)
            {
                errors.Add("Please enter your message. It should have at least 15 characters.");
            }

            string response = "";

            if (!errors.Any())
            {
                // Subject
                if (string.IsNullOrEmpty(subject))
                {
                    subject = "Contact Form Submission";
                }

                var mail = new System.Net.Mail.MailMessage
                {
                    From = new System.Net.Mail.MailAddress(email),
                    Subject = subject,
                    IsBodyHtml = true,
                    Body = "<strong>Email from:</strong> " + name + "<br/><br/><br/>" + contact_message + "<br/><br/><br/><hr/><br/>" +
                            "This email was sent from Ognit contact form."
                };

                mail.Headers.Add("From", name + " <" + email + ">");
                mail.Headers.Add("Reply-To", email);
                mail.Headers.Add("MIME-Version", "1.0");
                mail.Headers.Add("Content-Type", "text/html; charset=ISO-8859-1");
                mail.To.Add("info@ognit.com");

                try 
                {
                    var smtp = new System.Net.Mail.SmtpClient { Host = "mail.ognit.com" };
                    smtp.Send(mail);

                    response = "OK";
                } 
                catch (Exception ex) 
                {
                    response = "Something went wrong. Please try again.";
                }
            }
            else
            {
                response = string.Join("<br/> \n", errors);
            }

            HttpContext.Current.Response.Write(response);
        }
    }
</script>